import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;
import java.util.Random;

public class KafkaProducerExample {

    private final static String TOPIC = "test";
    private final static String BOOTSTRAP_SERVERS =
            "localhost:9092,localhost:9093,localhost:9094";
    private final static int MOVIES_COUNT = 1000;
    private final static int NORMIES_COUNT = 1000;
    private final static int TROLLS_COUNT = 100;
    public final static int SEQ_LENGTH = 100000;

    private Producer<Long, String> createProducer() {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaProducerExample");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        return new KafkaProducer<>(properties);
    }

    /**
     * Generates random users.
     * @param normies: number of normies.
     * @param trolls: number of trolls.
     * @return An ArrayList of Users.
     */
    private ArrayList<User> generateUsers(int normies, int trolls) {
        ArrayList<User> all = new ArrayList<>();
        for (int i = 0; i < normies; i++) {
            NormieUser normie = new NormieUser(i);
            all.add(normie);
        }
        for (int i = normies; i < normies + trolls; i++) {
            all.add(new TrollUser(i));
        }
        Collections.shuffle(all);
        return all;
    }

    /**
     * Generates random movies.
     * @param count: Number of movies to generate.
     * @return: An ArrayList of movies.
     */
    private ArrayList<Movie> generateMovies(int count) {
        ArrayList<Movie> all = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Movie movie = new Movie(i);
            all.add(movie);
        }
        Collections.shuffle(all);
        return all;
    }

    public void runProducer(final int sendMessageCount) throws Exception {
        final Producer<Long, String> producer = createProducer();
        long time = System.currentTimeMillis();

        ArrayList<Movie> movies = generateMovies(MOVIES_COUNT);
        ArrayList<User> users = generateUsers(NORMIES_COUNT, TROLLS_COUNT);

        try {
            Random random = new Random();
            for (long index = time; index < time + sendMessageCount; index++) {
                Movie movie = movies.get(random.nextInt(movies.size()));
                User user = users.get(random.nextInt(users.size()));
                int rating = user.rateMovie(movie);
                String userIdStr = Integer.toString(user.getId());
                String clipIdStr = Integer.toString(movie.getId());
                String ratingStr = Integer.toString(rating);

                final ProducerRecord<Long, String> record =
                        new ProducerRecord<>(TOPIC, index,
                                String.join(",", userIdStr, clipIdStr, ratingStr));

                producer.send(record, (metadata, exception) -> {
                    long elapsedTime = System.currentTimeMillis() - time;
                    System.out.printf("sent record(key=%s value=%s) " +
                                    "meta(partition=%d, offset=%d) time=%d\n",
                            record.key(), record.value(), metadata.partition(),
                            metadata.offset(), elapsedTime);
                });

            }
        } finally {
            producer.flush();
            producer.close();
        }
    }

}
