public class Main {
    public static void main(String... args) throws Exception {
        KafkaProducerExample kafkaProducerExample = new KafkaProducerExample();
        if (args.length == 0) {
            kafkaProducerExample.runProducer(KafkaProducerExample.SEQ_LENGTH);
        } else {
            kafkaProducerExample.runProducer(Integer.parseInt(args[0]));
        }
    }
}
