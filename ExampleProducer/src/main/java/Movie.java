import java.util.Random;

public class Movie {
    // Movie identification number.
    private int id;

    // An "objective" rating of the movie.
    private int trueRating;

    // Random number generator, must be deterministically initialized based on movie id.
    private Random random = null;

    /**
     * Creates an instance of a movie from its id.
     * @param id: Movie identification number.
     */
    public Movie(int id) {
        this.id = id;
        this.random = new Random(id);
        this.trueRating = this.random.nextInt(10) + 1;
    }

    public int getId() {
        return id;
    }

    public int getTrueRating() {
        return trueRating;
    }
}
