import java.util.Random;

import static java.lang.Math.*;

public class NormieUser implements User {
    // The user's id. It determines the random seed.
    private int id;

    // Random number generator, with seed determined by user's id.
    private Random random = null;

    @Override
    public int getId() {
        return this.id;
    }

    public NormieUser(int id) {
        this.id = id;
        this.random = new Random(this.id);
    }

    @Override
    public int rateMovie(Movie movie) {
        int trueRating = movie.getTrueRating();
        int deviation = (int) round(this.random.nextGaussian());
        int myRating = min(10, max(1, trueRating + deviation));
        return myRating;
    }
}
