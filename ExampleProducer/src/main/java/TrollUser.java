public class TrollUser implements User {
    // The user's id
    private int id;

    public TrollUser(int id) {
        this.id = id;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public int rateMovie(Movie movie) {
        int trueRating = movie.getTrueRating();
        if (trueRating <= 5) {
            return 10;
        } else {
            return 1;
        }
    }
}
