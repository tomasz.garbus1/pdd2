public interface User {
    /**
     * Gets the user's id.
     * @return User's id.
     */
    int getId();

    /**
     * Rates the movie in accordance with the user's personality (e.g. troll, normal person, connoisseur).
     *
     * @param movie: A movie to rate.
     * @return User's rating of the movie
     */
    int rateMovie(Movie movie);
}
