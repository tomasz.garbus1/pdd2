# PDD2

## Zadanie 2
```
Obsługujemy serwis udostępniania, oglądania i oceniania filmów w internecie.
Serwis ten generuje strumień danych dotyczących odtworzeń zawierający
identyfikator użytkownika, identyfikator filmu i ocenę użytkownika w skali 1-10

Od niedawna zaczęliśmy dostrzegać oceny pochodzące od botów lub troli. Aby je
zwalczać potrzebujemy zidentyfikować użytkowników oceniających skrajnie (1, 10)
i jednocześnie różniących się co najmniej o 5 od mediany ocen filmu.

Chcielibyśmy aby w ciągu dnia dane zbierały się do HDFS-a, a w nocy, kiedy
użycie systemu jest najmniejsze, będziemy uruchamiali analizator.

Zaimplementuj w wybranych przez siebie narzędziach dwa programy.

Pierwszy obsługujący strumień odtworzeń i ocen elementów w formacie
(USER_ID, CLIP_ID, RATING), który rozsądnie wybierze 3% danych i zapisze je w
HDFS. Załóż, że strumień pochodzi z Kafki. Ze względu na prędkość strumienia,
chcemy aby zapisywanie danych odbywało się w sposób rozproszony i skalowalny.

Drugi program powinien czytać dane z HDFS i wyliczać na ich podstawie zbiór
osób podejrzanych o bycie trolem. Implementacja algorytmu wybierającego powinna
spełniać definicję algorytmów minimalnych.
```

## Rozwiązanie - design
Rozwiązanie zakłada obecność katalogów `hadoop-3.1.2` i `kafka_2.1.2-2.2.0`. W skryptach
`run_hdfs.sh`, `stop_hdfs.sh`, `run_kafka.sh` i `stop_kafka.sh` należy zmienić stałe
`HADOOP_HOME` i `KAFKA_HOME` na własne lokacje Hadoopa i Kafki.

### Producent przykładowych danych
Zaimplementowałem przykładowy generator danych w katalogu `ExampleProducer`.
Wysyła on przez noc 100000 ocen filmów, 1000 użytkowników jest "normalnych", 100 to trolle.

### Konsumer strumienia
Program znajduje się w katalogu `StreamReader` i jest kodem w Javie. Uruchamiam go przez załadowanie
projektu do IntelliJ idea, więc niestety nie wiem, jak zbudować go z konsoli, ale chyba jest to zwykły
projekt w gradle.

Konsumer dostaje z Kafki kolejne krotki `userId,clipId,rating` (w dokładnie tym formacie).
Z nich wybiera 3% krotek do zapisu w HDFS w ten sposób, że krotka zostaje zapisana wtw `clipId % 100 < 3`.
Implementacja konsumera w javie jest rozproszona i skalowalna - może działać równolegle tyle programów,
ile jest skonfigurowanych partycji w Kafce. W swoich przykładach zakładałem 3 partycje, co znajduje też
odzwierciedlenie w stałych w kodzie konsumera. Aby uruchomić więcej instancji naraz, należy te
stałe zmienić.

Nowe krotki zapisywane są w HDFS, każda w osobnym pliku, w katalogu `/user/hdfs/new_ratings`.
Nazwa pliku to `userId,clipId,rating` a jego cała treść to `1`.

### Drugi program
Drugi program jest notatnikiem zeppelinowym `pdd2.json`. Jego działanie składa się z następujących faz:

* wczytanie plików z katalogu `/user/hdfs/new_ratings` do rozproszonego RDD
* zliczenie wystąpień dla każdej pary `clipId,rating` i zsumowanie tych wartości z uprzednio
zapisanymi w katalogu `/user/hdfs/aggregated`.
* aktualizacja plików w `/user/hdfs/aggregated`
* obliczenie median ocen dla filmów. Mając dla każdego filmu 10 wartości z poprzednich faz, tzn.
liczbę ocen dla każdej oceny od 1 do 10, medianę liczy się już w czasie stałym per film.
* filtrowanie trolli. Odbywa się to przez zbroadcastowanie mapy median do wszystkich partycji
(zakładam tu, że filmów jest o rząd wielkości mniej, niż ratingów), a następnie przefiltrowanie RDD
z nowymi ratingami
* opróżnienie katalogu `/user/hdfs/new_ratings`

Przy każdej fazie zadbałem o to, aby spełniała warunki algorytmu minimalnego, w komentarzach w kodzie
znajdują się krótkie uzasadnienia.