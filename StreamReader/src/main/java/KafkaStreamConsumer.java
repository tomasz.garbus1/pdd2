import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;

import java.util.Collections;
import java.util.Properties;

public class KafkaStreamConsumer {
    private final static String TOPIC = "test";
    private final static String BOOTSTRAP_SERVERS =
            "localhost:9092,localhost:9093,localhost:9094";


    FileSystem fs;

    KafkaStreamConsumer() throws IOException {
        Configuration conf = new Configuration();
        // Set FileSystem URI
        String hdfsuri = "hdfs://localhost:9000";
        conf.set("fs.defaultFS", hdfsuri);
        // Because of Maven
        conf.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
        conf.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
        // Set HADOOP user
        System.setProperty("HADOOP_USER_NAME", "hdfs");
        System.setProperty("hadoop.home.dir", "/");
        //Get the filesystem - HDFS
        this.fs = FileSystem.get(URI.create(hdfsuri), conf);
    }

    private void writeEntryToHdfs(String str) throws IOException {
        //Create a path
        Path hdfswritepath = new Path("hdfs://localhost:9000/user/hdfs/new_ratings/ " + str);
        //Init output stream
        FSDataOutputStream outputStream = this.fs.create(hdfswritepath);
        //Cassical output stream usage
        outputStream.writeBytes("1");
        outputStream.close();
    }

    private Consumer<Long, String> createConsumer() {
        final Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "KafkaStreamConsumer");
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class.getName());

        // Creates the consumer using |properties|.
        final Consumer<Long, String> consumer =
                new KafkaConsumer<>(properties);

        // Subscribes to the topic.
        consumer.subscribe(Collections.singletonList(TOPIC));
        return consumer;
    }

    public void runConsumer() {
        final Consumer<Long, String> consumer = createConsumer();

        int giveUp = 50;
        int noRecordsCount = 0;

        while (true) {
            final ConsumerRecords<Long, String> consumerRecords = consumer.poll(1000);

            if (consumerRecords.count() == 0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }

            consumerRecords.forEach(record -> {
                System.out.printf("Consumer Record:(%d, %s, %d, %d)\n", record.key(), record.value(),
                        record.partition(), record.offset());
                try {
                    // Select only 3% movies.
                    int movieId = Integer.parseInt(record.value().split(",")[1]);
                    if (movieId % 100 < 3) {
                        this.writeEntryToHdfs(record.value());
                    }
                } catch (IOException | NumberFormatException | ArrayIndexOutOfBoundsException e) {} finally {}
            });

            consumer.commitAsync();
        }
        consumer.close();
        System.out.println("DONE");
    }

}
