public class Main {
    public static void main(String... args) throws Exception {
        KafkaStreamConsumer kafkaStreamConsumer = new KafkaStreamConsumer();
        kafkaStreamConsumer.runConsumer();
    }
}
