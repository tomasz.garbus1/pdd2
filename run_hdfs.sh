export HADOOP_HOME=/home/tomek/Desktop/PDD2/hadoop-3.1.2
ssh-add ~/.ssh/id_rsa
ssh-agent
export PDSH_RCMD_TYPE=ssh
${HADOOP_HOME}/bin/hdfs namenode -format
${HADOOP_HOME}/sbin/start-dfs.sh
${HADOOP_HOME}/bin/hdfs dfs -mkdir /user
${HADOOP_HOME}/bin/hdfs dfs -mkdir /user/hdfs
# Tu będą lądować nowe rating podczas zbierania danych w nocy.
${HADOOP_HOME}/bin/hdfs dfs -mkdir /user/hdfs/new_ratings
# Tu będą zsumowane wartości po kluczach (clipId,rating).
${HADOOP_HOME}/bin/hdfs dfs -mkdir /user/hdfs/aggregated
${HADOOP_HOME}/bin/hdfs dfs -chmod -R 777 /user/hdfs